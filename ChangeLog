2008-04-01  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* INSTALL: added gconf-sharp dependency
	* NEWS: update news for 0.6.99 release
	* configure.ac: bump version to 0.6.99

2008-03-31  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* src/Settings.cs: don't explode when the gconf
	keys do not exist.

2008-03-28  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* data/hipo.schemas.in:
	* src/HipoMainWindow.cs:
	* src/Settings.cs:
	* src/TracksContainer.cs:
	* src/hipo.glade:

	Add tracks view column editing.
	Fixes #484512 and #392427

2008-03-20  Andres Herrera  <andres@usr.cl>

        * src/TracksSource.cs: Fix Obsolete taglib functions 
        warnings. closes #522535

2008-03-19  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* src/ipod-sharp.dll.config.in: delete as it is not
	needed anymore due to the libipoddevice dependency removal.

2008-03-15  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* configure.ac: Pass make distcheck.

2008-03-14  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* src/DeviceSource.cs:
	* src/Makefile.am:
	* src/PlaylistSource.cs:
	* src/Source.cs:
	* src/SourceList.cs:
	* src/TracksContainer.cs:
	* src/devicepopup.xml:
	* src/playlistpopup.xml:
	* src/trackpopup.xml:

	Clean the (Add|Remove)Handlers mess, it was a bug nest.
        The popup menus are now generated in the code.

2008-03-14  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* configure.ac:
	* data/Makefile.am:
	* data/hipo.schemas.in:
	* src/HipoMainWindow.cs:
	* src/Makefile.am:
	* src/Settings.cs:
	* src/Tools.cs: logging fixes
	* src/TracksSource.cs: logging fixes

	More logging.
	Add gconf support and remember the last path
	opened in the filechooser.

2008-03-14  Benoit Garret  <benoit.garret_gnome@gadz.org>

        * src/PlaylistSource.cs:
        * src/SourceList.cs:
        * src/Tools.cs:
        * src/TracksContainer.cs:

        Fix a stupid mistake in the logging.
        Add more comments.

2008-03-14  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* src/DeviceSource.cs:
	* src/EditableCellRenderer.cs:
	* src/HipoMain.cs:
	* src/HipoMainWindow.cs:
	* src/ImportDialog.cs:
	* src/PlaylistSource.cs:
	* src/Saviour.cs:
	* src/Source.cs:
	* src/SourceList.cs:
	* src/Tools.cs:
	* src/TracksContainer.cs:
	* src/TracksSource.cs:

	More logging.

2008-03-06  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* src/CapacityBar.cs:
	* src/DeviceSource.cs:
	* src/EditableCellRenderer.cs:
	* src/HipoMain.cs:
	* src/HipoMainWindow.cs:
	* src/Makefile.am:
	* src/PlaylistSource.cs:
	* src/PlaylistView.cs:
	* src/Saviour.cs:
	* src/Source.cs:
	* src/SourceList.cs:
	* src/Tools.cs:
	* src/TracksContainer.cs:
	* src/TracksSource.cs:
	* src/TracksView.cs:
	* src/devicepopup.xml:
	* src/errordialog.glade:
	* src/hipo.glade:
	* src/hipo.xml:
	* src/menubar.xml:
	* src/playlistpopup.xml:
	* src/trackpopup.xml:

	Big Bad Redesign

2008-03-03  Basilio Kublik <basilio@gnome.cl>

	* src/errordialog.glade: unset translatable property to stock button

2008-02-20  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* INSTALL: added taglib-sharp dependency
	* Makefile.am:
	* NEWS: update for the 0.6.1 release
	* configure.ac: bump version to 0.6.1
	* src/Makefile.am:
	* taglib-sharp/Makefile.am:

2008-02-20  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* INSTALL: modified dependencies
	* NEWS: update news for 0.6 release.
	* configure.ac: bump to version 0.6.

2008-02-17  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* src/HipoMain.cs: catch glib exceptions

2008-02-17  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* configure.ac:
	* src/HipoMainWindow.cs:
	* src/Makefile.am:

	Build with the newest ipod-sharp. Dependencies now include
	ipod-sharp >= 0.8.0 and ndesk-dbus-glib >= 0.3

	Fixes #500761

2008-01-11  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* src/TracksView.cs: make AddFolder return the
	correct value.

	Fixes #508576

2007-12-16  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* src/HipoMain.cs: catch exceptions and present
	them in a popup.
	* src/HipoMainWindow.cs:
	* src/Makefile.am:
	* src/errordialog.glade:

	(Basilio Kublik, Benoit Garret)

	Fixes #472243

2007-12-03  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* src/HipoMainWindow.cs: clean up my mess, we do
	want to build against ipod-sharp svn yet as it would
	add a dependency on ndesk-dbus-glib.

2007-12-02  Benoit Garret  <benoit.garret_gnome@gadz.org>

	* src/HipoMainWindow.cs: allow compiling with
	ipod-sharp svn (rev 90518).

2007-10-11  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: Use a TreeModelFilter for the
	tracks view.

	(Diego Escalante, Benoit Garret, Basilio Kublik)

	Fixes #452472

2007-10-11  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: Lot of code clean up.
	* src/PlaylistView.cs:
	* src/TracksView.cs:  

	(Benoit Garret)

	Fixes #485039

2007-10-11  Pedro Villavicencio Garrido  <pvillavi@gnome.org>
	
	* src/HipoMainWindow.cs: Incorrect save behavior.
	* src/PlaylistView.cs: Playlist track number sometimes
	not updated.

	(Benoit Garret)

	Fixes #485140 and #485141

2007-10-07  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: Edit multiple tracks at once.
	* src/hipo.glade:
	
	(Benoit Garret)

	Fixes #483897.
	
2007-10-05  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* Makefile.am: Added documentation.
	* configure.ac:
	* src/Defines.cs.in:
	* src/HipoMainWindow.cs:

	Fixes #395885.

2007-09-29  Pedro Villavicencio Garrido  <pvillavi@gnome.org>
	
	* NEWS: update news for 0.5 release.
	* configure.ac: bumping version to 0.5.

2007-09-29  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs:
	* src/ImportDialog.cs: fixed some translation issues. (Andre Klapper)
	
	Fixes #475574.

2007-09-26  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: use casting instead of System.Double.
	(Benoit Garret).

	Fixes #479722.

2007-09-21  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: fixed ipod capacity bar for
	ipod's with less than 1GB of capacity. (Felipe Barros)

	Fixed #478826.

2007-09-16  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: Make the playlists name editables.
	(Benoit Garret)
	* src/PlaylistView.cs:
	* src/hipo.xml:

	Fixed #440436.

2007-09-16  Pedro Villavicencio Garrido <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: Fixed some string issues.
	(Andre Klapper)

2007-09-16  Pedro Villavicencio Garrido <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: Fixed keyboard shortcuts
	(Benoit Garret).

	Fixed #452474.

2007-05-19  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: make use of ellipsizemode.
	set the number of tracks color to grey.
	* src/PlaylistView.cs: 
	* src/hipo.glade: 

2007-04-29  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: now you can drag and drop
	from the track list to your desktop.

	Fixed #434396

2007-04-28  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/TracksView.cs: correct the audioproperties method.

2007-04-28  Pedro Villavicencio  <pvillavi@gnome.org>

	* configure.ac: bumping ipod-cil library to 0.6.3 since
	it corrected a couple of fixes.

	Closes #433047

2007-04-14  Pedro Villavicencio  <pvillavi@gnome.org>

	* taglib-sharp/Makefile.am: modified the makefile 
	for working with the new version of taglib-sharp.

2007-04-04  Pedro Villavicencio  <pvillavi@gnome.org>
	
	* data/Makefile.am: add ipod.gif
	* src/HipoMainWindow.cs: use ImportDialog instead of
	ProgressDialog. We want to have a translatable progress 
	dialog.
	* src/ImportDialog.cs: add file.
	* src/Makefile.am: 

	Fixed #426254, Thanks to Felipe Barros.

2007-03-29  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs:
	* src/TracksView.cs: Implement D&D from nautilus.
	Also some code clean up.

	Fixed #419723

2007-03-17  Pedro Villavicencio  <pvillavi@gnome.org>

	* NEWS: updated news for 0.4 release.
	* configure.ac: bump version to 0.4

2007-03-13  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: fixed sorting method and 
	remove unnecessary code.

2007-03-13  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/TracksView.cs: commented out for now the sort function.

2007-03-13  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* autogen.sh:
	* configure.ac: Use new intltool syntax.

2007-03-11  David Lodge <dave@cirt.net>

	* configure.ac: Added en_GB to ALL_LINGUAS

2007-03-11  Pedro Villavicencio  <pvillavi@gnome.org>

	* hipo.desktop.in: remove 'Application' category
	because is not valid in the freedesktop specification.

2007-03-10  Ilkka Tuohela  <hile@iki.fi>

	* configure.ac: Added fi to ALL_LINGUAS

2007-02-27  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: Make sure at least one track 
	is selected when showing the popup menu. (Benoit Garret)

	Fixed #412792

2007-02-27  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* autogen.sh: remove update-checkout.sh.
	* taglib-sharp/update-checkout.sh: deleted.
	* taglib-sharp/: set the checkout of the taglib-sharp
	as a svn external property.

2007-02-25  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs:
	* src/hipo.xml: Add device properties to the menu.

2007-02-18  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: Add support for m4a files.
	* src/TracksView.cs:
	
	Fixed #409097

2007-02-18  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: Add metadata edit
	in the tracks view. (Benoit Garret)

	Closes #408808

2007-02-17  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/hipo.glade: Add shadows around the playlist 
	view and the tracks view. (Benoit Garret).

	Closes #408806

2007-02-14  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: Fix a typo where the artist field 
	was saved both in the artist and the album field,

	* src/TracksView.cs: Update list of tracks
	when a change is made in the properties window.

	Fixed #407720
	
	Thanks to Benoit Garret <benoit.garret_gnome@gadz.org>

2007-02-07  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: update copyrights and 
	add the hipo webpage to the about dialog.

	* src/PlaylistView.cs:
	* src/TracksView.cs: add recursive import.
	closes #402515. (Felipe Barros)

2007-02-06  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: fixed percentage calculation
	closes #402536, fixed #402940. (Felipe Barros).

2007-01-22  Alessio Frusciante  <algol@firenze.linux.it>

	* configure.ac: Added "it" (Italian) to ALL_LINGUAS.

2007-01-21  Pedro Villavicencio  <pvillavi@gnome.org>

	* configure.ac: add Brazilian Portuguese translation.
	Thanks to Jose Ricardo Anacleto Cardozo <ricky.devel@gmail.com>

2007-01-21  Pedro Villavicencio  <pvillavi@gnome.org>

	* NEWS: update news for 0.3 release
	* configure.ac: bumping version to 0.3
	* src/HipoMainWindow.cs: adding Felipe Barros to
	authors field.

2007-01-21  Pedro Villavicencio  <pvillavi@gnome.org>

	* configure.ac: Added Polish translation,
	thanks to Tomasz Dominikowski <dominikowski@gmail.com>.	

2007-01-16  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: change treeview to multiple selection mode.
	correct a couple of issues regarded to the progress dialog.

	* src/PlaylistView.cs: 
	* src/TracksView.cs: update functions for working with the new code.
	
2007-01-16  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs:
	* src/TracksView.cs: Add progress dialog.

	Closes: #397251

2007-01-10  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs:
	* src/hipo.glade: extend device properties.

	Closes: #394804

2007-01-09  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: improve size calculation functions. (Felipe
	Barros)
	
	Closes: #392478

2007-01-09  Pedro Villavicencio  <pvillavi@gnome.org>

	* configure.ac: add Dutch translation,
	 thanks to Max Beauchez <max.780@gmail.com>

2007-01-09  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: fixed glade translation issues.

	Closes: #394619

2007-01-09  Pedro Villavicencio  <pvillavi@gnome.org>

	* configure.ac: bump version to 0.2 .
	* NEWS: update news.

2007-01-08  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/TracksView.cs: Checks for null parameters in sort function.

	Closes: #393968

2007-01-08  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: Ipod actions greyed out when no 
	device is connected, this was our first GNOME Love bug. (Thanks to Gabriel Felipe Cornejo <gnomeusr@gmail.com>)

	Closes: #392432

2007-01-04  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: 
	* src/TracksView.cs: clear stores when device is not present.
	Closes: #392533

2007-01-03  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: add function to manage the device
	properties dialog, also fix a compilation issue in Archlinux (thanks
	to Eugenia Loli Queru for this report).
	* src/PlaylistView.cs:
	* src/hipo.glade: add dialog for show the device properties.
	* src/hipo.xml: add device popup.  
	
	(Closes: #392430)

2007-01-03  Pedro Villavicencio  <pvillavi@gnome.org>

	* taglib-sharp/update-checkout.sh: changed
	svn for http in the anonymous svn access.
	thanks to felipe barros. (Closes: #392482)

2007-01-03  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* configure.ac:
	* src/Makefile.am:
	* src/ipod-sharp.dll.config.in: add for
	missing ipoddevice config.

2007-01-02  Pedro Villavicencio  <pvillavi@gnome.org>

	* configure.ac: add Catalan translation,
	thanks to Gil Forcada <gilforcada@guifi.net>.

2007-01-01  Pedro Villavicencio  <pvillavi@gnome.org>

	* Release 0.1

	* INSTALL: 
	* NEWS: update for 0.1
	* configure.ac: bump version to 0.1

2006-12-26  Pedro Villavicencio  <pvillavi@gnome.org>

	* Makefile.am: add new directory.
	* autogen.sh: run update-checkout.sh.
	* configure.ac: remove dependencie.
	* src/Makefile.am: replace resource of taglib-sharp.
	* taglib-sharp/Makefile.am:
	* taglib-sharp/update-checkout.sh: checks for newer taglib-sharp.
	
	copied from Banshee.

2006-12-26  Pedro Villavicencio  <pvillavi@gnome.org>

	* data/Makefile.am:
	* data/hipo-logo.png:
	* src/HipoMainWindow.cs:
	* src/Makefile.am: add a cute logo, thanks to
	Carlos Candia Flores for the awesome vectorization
	and Gabriel Bustos for it.

2006-12-23  Pedro Villavicencio  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs:
	* src/PlaylistView.cs:
	* src/TracksView.cs: code re order.

2006-12-18  Pedro Villavicencio  <pvillavi@gnome.org>

	* configure.ac:
	* po/POTFILES.in:
	* po/es.po:  Added Spanish Translation. (Felipe Barros).
	* src/HipoMainWindow.cs: 
	* src/hipo.glade:
	* src/hipo.xml: Add dialog for edit tags and cover arts.
	Add Drag & Drop support (Felipe Barros).

2006-12-12  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* Makefile.am:
	* configure.ac:
	* data/Makefile.am:
	* data/multimedia-player-ipod-U2-color.png:
	* data/multimedia-player-ipod-U2-monochrome.png:
	* data/multimedia-player-ipod-mini-blue.png:
	* data/multimedia-player-ipod-mini-gold.png:
	* data/multimedia-player-ipod-mini-green.png:
	* data/multimedia-player-ipod-mini-pink.png:
	* data/multimedia-player-ipod-mini-silver.png:
	* data/multimedia-player-ipod-nano-black.png:
	* data/multimedia-player-ipod-nano-white.png:
	* data/multimedia-player-ipod-shuffle.png:
	* data/multimedia-player-ipod-standard-color.png:
	* data/multimedia-player-ipod-standard-monochrome.png:
	* data/multimedia-player-ipod-video-black.png:
	* data/multimedia-player-ipod-video-white.png:
	* src/HipoMainWindow.cs:
	* src/PlaylistView.cs: add Tango icons for each iPod model.

2006-12-10  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: Add eject option to the quit dialog.

2006-12-05  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/Makefile.am:
	* src/Defines.cs.in:  Added support for translations.
	* src/PlaylistWindow.cs: Use Catalog.GetString, every string
	that goes in the UI should use it.

2006-11-30  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: 
	* src/PlaylistView.cs: add some new functions to manage
	the Playlists.
	* hipo.xml: changed names of the popups.

2006-11-28  Pedro Villavicencio Garrido  <pvillavi@gnome.org>
	
	* src/HipoMainWindow.cs: added popup to manage the playlists. 
	* src/PlaylistView.cs: code re-order.

	* src/HipoMainWindow.cs: 
	* src/hipo.glade: Added functions to show 
	the current size of the device. 
	(thanks to Felipe Barros)

2006-11-27  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs:
	* src/hipo.xml: 
	* src/hipo.glade: 
	* src/PlaylistView.cs: started the implementation of 
	Playlists.

2006-11-25  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: 
	* src/hipo.xml: 
	* src/hipo.glade: Remove the old menubar, migration to 
	GtkUIManager.
	
2006-11-20  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: make use of threads.
	* src/Tracker.cs: add missing tracknumber.

2006-11-16  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* configure.ac: add dependency on taglib-sharp.

	* src/hipo.glade:
	* src/HipoMainWindow.cs: 
	* src/Tracker.cs: rework and order of the code.
	
2006-11-14  Pedro Villavicencio Garrido  <pvillavi@gnome.org>

	* src/HipoMainWindow.cs: Initial import of hipo into GNOME CVS.
	* src/HipoMainWindow.cs (OnButtonPress), (PopupMenu),
	(OnPopupMenu) : add popup menu to the view.
	* src/hipo.glade: remove old dialog.
