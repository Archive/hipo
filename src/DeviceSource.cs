// DeviceSource.cs
//
//  Copyright (C) 2008 Benoit Garret
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using System.Collections.ObjectModel;
using Glade;
using Gtk;
using IPod;
using Mono.Unix;

namespace Hipo
{
	public class DeviceSource : SourceBase
	{
		[Widget] Window deviceProperties;
		[Widget] Viewport deviceViewport;
		[Widget] Label serialLabel;
		[Widget] Label modelLabel;
		[Widget] Label generationLabel;
		[Widget] Label capacityLabel;
		[Widget] Label mountLabel;
		[Widget] Label spaceLabel;
		[Widget] Label firmwareLabel;
		
		private Device device;
		private Menu menu;
		
		public DeviceSource (Device device)
		{
			Tools.Log (this, 1, "Creating device source for {0}", device.Name);
			
			this.device = device;
			
			this.Type = SourceType.Device;
			this.Parent = SourceType.None;
			this.Icon = IconTheme.Default.LoadIcon (device.ModelInfo.IconName, 16, 0);
			
			Glade.XML xml = new Glade.XML (null, "hipo.glade", "deviceProperties", null);;
			xml.Autoconnect (this);
			
			serialLabel.Text = this.device.ProductionInfo.SerialNumber;
			modelLabel.Text = this.device.ModelInfo.DeviceClass;
			generationLabel.Text = String.Format ("{0}", this.device.ModelInfo.Generation);
			capacityLabel.Text = Tools.FormatString ((double) this.device.VolumeInfo.Size);
			mountLabel.Text = this.device.VolumeInfo.MountPoint;
			spaceLabel.Text = String.Format ("{0}%", (int) ((double) device.VolumeInfo.SpaceUsed / (double) device.VolumeInfo.Size * 100));
			firmwareLabel.Text = this.device.FirmwareVersion;
			
			// remove the viewport from the window so we can add it elsewhere
			deviceProperties.Remove (deviceViewport);
			
			// menu creation
			menu = new Menu();
			
			MenuItem item = (MenuItem)(new Action ("RenameDevice",
			                                       Catalog.GetString ("Rename"),
			                                       Catalog.GetString ("Rename the device"),
			                                       Gtk.Stock.Copy)).CreateMenuItem ();
			item.Activated += OnRename;
			menu.Add (item);
		}
		
		public override string Name
		{
			get {
				return device.Name;
			}
			set {
				if (device.Name != value)
				{
					device.Name = value;
					Saviour.Instance.Save ();
				}
			}
		}
		
		public override Widget View
		{
			get {
				return deviceViewport;
			}
		}
		
		public override Menu Menu
		{
			get {
				return menu;
			}
		}
		
		public void Eject ()
		{
			this.device.Eject ();
		}
		
		public ReadOnlyCollection<ArtworkFormat> LookupArtworkFormats (ArtworkUsage usage)
		{
			return this.device.LookupArtworkFormats (usage);
		}
		
		private void OnRename (object o, EventArgs args)
		{
			Dialog dialog = new Dialog (Catalog.GetString ("Rename the device"), null, Gtk.DialogFlags.DestroyWithParent);
			
			dialog.Modal = true;
			dialog.AddButton ("Cancel", ResponseType.Close);
			dialog.AddButton ("OK", ResponseType.Ok);
			
			Gtk.Entry entry = new Gtk.Entry (this.Name);
			dialog.VBox.PackStart (entry, false, false, 5);
			dialog.DefaultResponse = ResponseType.Ok;
			
			dialog.ShowAll ();
			ResponseType r = (ResponseType) dialog.Run ();
			
			if (r == ResponseType.Ok)
			{
				this.Name = entry.Text;
			}
			
			Tools.Log (this, 3, "Device renamed to {0}", this.Name);
			
			dialog.Destroy ();
		}
	}
}
