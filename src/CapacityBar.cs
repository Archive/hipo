// CapacityBar.cs
//
//  Copyright (C) 2008 Benoit Garret
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using IPod;
using Gtk;
using Mono.Unix;

namespace Hipo
{
	public class CapacityBar : ProgressBar
	{
		private Device device = null;
		
		public Device Device
		{
			set {
				if (device != null)
				{
					device.Changed -= OnChanged;
					device.TrackDatabase.SaveEnded -= OnChanged;
				}
				
				device = value;
				
				if (device != null)
				{
					device.Changed += OnChanged;
					device.TrackDatabase.SaveEnded += OnChanged;
					Refresh ();
				}
				else
				{
					Clear ();
				}
			}
		}
		
		private void Refresh ()
		{
			string capacity = null;
			string used = null;
			double percent;
			
			capacity = Tools.FormatString ((double) device.VolumeInfo.Size);
			used = Tools.FormatString ((double) device.VolumeInfo.SpaceUsed);
			
			percent = ((double) device.VolumeInfo.SpaceUsed * 100) / ((double) device.VolumeInfo.Size);
			
			Gtk.Application.Invoke (delegate {
				
				/* space used in the device, example: used 15G of 60G */ 
				this.Text = String.Format (Catalog.GetString ("Using {0} of {1}"), used, capacity);
				this.Fraction = percent  / 100;
			});
		}
		
		private void Clear ()
		{
			Gtk.Application.Invoke (delegate {
				
				this.Text = Catalog.GetString ("No iPod Found");
				this.Fraction = 0.0;
			});
		}
		
		private void OnChanged (object o, EventArgs args)
		{
			Refresh ();
		}
	}
}
