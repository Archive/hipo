// SourceList.cs
//
//  Copyright (C) 2008 Benoit Garret
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using System.Collections.Generic;
using Gtk;
using IPod;

namespace Hipo
{
	public class SourceList
	{
		private static SourceList instance = null;
		
		private TreeView view;
		private TreeStore store;
		private Menu menu;
		private List<ISource> sources;
		private ISource selectedSource;
		
		private static TargetEntry [] dest_entries = {
			new TargetEntry ("application/x-hipo", 0, 0)
		};
		
		public event SelectionChangedEventHandler SelectionChanged;
		public delegate void SelectionChangedEventHandler (object source, SelectionChangedArgs e);
		
		public class SelectionChangedArgs : EventArgs
		{
			ISource source;
			
			public SelectionChangedArgs (ISource source)
			{
				this.source = source;
			}
			
			public ISource Source
			{
				get {
					return source;
				}
			}
		}
		
		private SourceList ()
		{
			Tools.Log (this, 1, "Creating source list");
			
			// initialize playlist list
			sources = new List<ISource>();
			
			// TODO: find a way to work around the redrawing bugs
			// TODO: the count column goes off-limits when the count gains a number
			// create the columns, treeview, etc
			view = new TreeView ();
			view.HeadersVisible = false;
			
			CellRendererPixbuf pixbufCell = new CellRendererPixbuf ();
			TreeViewColumn column = new TreeViewColumn ();
			column.PackStart (pixbufCell, true);
			
			TreeCellDataFunc iconCellDataFunc = new TreeCellDataFunc (IconCellDataFunc);
			column.SetCellDataFunc (pixbufCell, iconCellDataFunc);
			
			column.Expand = false;
			column.Resizable = true;
			view.AppendColumn (column);
			
			
			EditableCellRenderer textCell = new EditableCellRenderer ();
			textCell.Edited += OnSourceEdit;
			textCell.Column = Column.Name;
			
			column = new TreeViewColumn ();
			column.PackStart (textCell, true);
			
			TreeCellDataFunc textCellDataFunc = new TreeCellDataFunc (TextCellDataFunc);
			column.SetCellDataFunc (textCell, textCellDataFunc);
			
			column.Expand = true;
			view.AppendColumn (column);
			
			
			CellRendererText countCell = new CellRendererText ();
			column = new TreeViewColumn ();
			column.PackStart (countCell, true);
			
			TreeCellDataFunc countCellDataFunc = new TreeCellDataFunc (CountCellDataFunc);
			column.SetCellDataFunc (countCell, countCellDataFunc);
			
			column.Expand = false;
			view.AppendColumn (column);
			
			// create the store
			store = new TreeStore (typeof (ISource));
			
			// bind store and view
			view.Model = store;
			
			// enable drag and drop
			view.EnableModelDragDest (dest_entries, Gdk.DragAction.Copy);			
			view.DragDataReceived += OnDragDataReceived;
			
			// hook up delegates
			view.Selection.Changed += OnSelectionChanged;
			
			// hook up popup menu handlers
			view.KeyPressEvent += OnKeyPress;
			view.ButtonPressEvent += OnButtonPress;
		}
		
		private void TextCellDataFunc (TreeViewColumn column, CellRenderer renderer,
		                               TreeModel model, TreeIter iter)
		{
			ISource source = (ISource)model.GetValue (iter, 0);
			EditableCellRenderer cell = (EditableCellRenderer)renderer;
			
			switch (cell.Column)
			{
			case Column.Name:
				cell.Text = source.Name;
				break;
			default:
				break;
			}
		}
		
		private void IconCellDataFunc (TreeViewColumn column, CellRenderer renderer,
		                               TreeModel model, TreeIter iter)
		{
			ISource source = (ISource)model.GetValue (iter, 0);
			CellRendererPixbuf cell = (CellRendererPixbuf)renderer;
			
			cell.Pixbuf = source.Icon;
		}
		
		private void CountCellDataFunc (TreeViewColumn column, CellRenderer renderer,
		                                TreeModel model, TreeIter iter)
		{
			ISource source = (ISource)model.GetValue (iter, 0);
			CellRendererText cell = (CellRendererText)renderer;
			
			cell.Foreground = "grey";
			
			if (source.Count < 1)
				cell.Text = "";
			else
				cell.Text = "(" + source.Count + ")";
		}
		
		public Widget View
		{
			get {
				return (Widget) view;
			}
		}
		
		public ISource SelectedSource
		{
			get {
				return selectedSource;
			}
			
			set {
				
				if (value != null)
					Tools.Log (this, 2, "Selecting source {0}", value.Name);
				else
					Tools.Log (this, 2, "No source selected");
				
				selectedSource = value;
				
				if (selectedSource != null)
				{
					store.Foreach (delegate (TreeModel model, TreePath path, TreeIter iter) {
						
						ISource source = (ISource)store.GetValue (iter, 0);
						
						if (source.Id == selectedSource.Id)
						{
							view.Selection.SelectIter (iter);
							return true;
						}
						else
							return false;
					});
					
					if (SelectionChanged != null)
					{
						SelectionChanged (this, new SelectionChangedArgs (selectedSource));
						Tools.Log (this, 2, "Source {0} selected", selectedSource.Name);
					}
				}
				else
				{
					if (SelectionChanged != null)
					{
						SelectionChanged (this, new SelectionChangedArgs (null));
						Tools.Log (this, 2, "No source selected");
					}
				}
			}
		}
		
		public DeviceSource Device
		{
			get {
				
				DeviceSource device = (DeviceSource)sources.Find (delegate (ISource source) {
					if (source.Type == SourceType.Device)
						return true;
					else
						return false;
				});
				
				return device;
			}
		}
		
		public TracksSource Tracks
		{
			get {
				
				TracksSource tracks = (TracksSource)sources.Find (delegate (ISource source) {
					if (source.Type == SourceType.Tracks)
						return true;
					else
						return false;
				});
				
				return tracks;
			}
		}
		
		public List<ISource> Playlists
		{
			get {
				
				return sources.FindAll (delegate (ISource source) {
					if (source.Type == SourceType.Playlist)
						return true;
					else
						return false;
				});
			}
		}
		
		public Menu Menu
		{
			get {
				return menu;
			}
		}
		
		public static SourceList Instance {
			
			get {
				
				if (instance == null)
					instance = new SourceList ();
				
				return instance;
			}
		}
		
		public void Add (ISource source)
		{
			Tools.Log (this, 3, "Adding source {0} with id {1} to the list", source.Name, source.Id);
			
			sources.Add (source);
			source.Deleted += OnSourceDeleted;
			
			if (source.Parent == SourceType.None)
				store.AppendValues (source);
			else
			{
				store.Foreach (delegate (TreeModel model, TreePath path, TreeIter iter) {
					
					ISource s = (ISource)model.GetValue (iter, 0);
					
					if (s.Type == source.Parent)
					{
						store.AppendValues (iter, source);
						return true;
					}
					else
						return false;
				});
			}
			
			Tools.Log (this, 3, "Added source {0} with id {1} to the list", source.Name, source.Id);
		}
		
		public void RefreshView ()
		{
			view.QueueDraw ();
		}
		
		public void Clear ()
		{
			this.store.Clear ();
			this.sources.Clear ();
		}
		
		public void ExpandAll ()
		{
			view.ExpandAll ();
		}
		
		private void OnSelectionChanged (object o, EventArgs args)
		{
			TreeIter iter;
			TreeModel model;
			
			if (((TreeSelection)o).GetSelected (out model, out iter))
			{
				ISource source = (ISource)model.GetValue (iter, 0);
				
				this.SelectedSource = source;
			}
		}
		
		[GLib.ConnectBefore]
		private void OnButtonPress (object o, ButtonPressEventArgs args)
		{
			// if right click then show the menu
			switch (args.Event.Button)
			{
			case 3:
				PopupMenu (args.Event);
				args.RetVal = true;
				break;
			}
		}
		
		private void OnKeyPress (object o, KeyPressEventArgs args)
		{
			switch (args.Event.Key)
			{
			case Gdk.Key.Menu:
				PopupMenu (args.Event);
				args.RetVal = true;
				break;
			}
		}
		
		private void PopupMenu (Gdk.Event ev)
		{
			Tools.Log (this, 3, "Showing popup menu for source {0}", selectedSource.Name);
			
			// build the menu and show it
			Menu menu = selectedSource.Menu;
			
			if (menu != null)
				menu.Popup (null, null, null,
				            ((ev != null) && ev.Type == Gdk.EventType.ButtonPress) ? ((Gdk.EventButton)ev).Button : 0,
				            Gdk.EventHelper.GetTime (ev));
		}
		
		private void OnSourceEdit (object o, EditedArgs args)
		{
			TreeIter iter = TreeIter.Zero;
			
			if (!store.GetIter (out iter, new TreePath (args.Path)))
				return;
			
			ISource source = (ISource)store.GetValue (iter, 0);
			
			source.Name = args.NewText;
		}
		
		private void OnSourceDeleted (object o)
		{
			Tools.Log (this, 2, "Removing source {0} from the source list");
			
			ISource source = (ISource)o;
			
			sources.Remove (source);
			store.Foreach (delegate (TreeModel model, TreePath path, TreeIter iter) {
				
				ISource s = (ISource)model.GetValue (iter, 0);
				
				if (source.Id == s.Id)
				{
					store.Remove (ref iter);
					return true;
				}

				return false;
			});
		}
		
		private void OnDragDataReceived (object o, DragDataReceivedArgs args)
		{
			TreePath treepath;
			TreeViewDropPosition position;
			TreeIter iter;
			
			view.GetDestRowAtPos (args.X, args.Y, out treepath, out position);
			store.GetIter (out iter, treepath);
			
			try {
				ISource source = (ISource) store.GetValue (iter, 0);
				
				if (source.Type == SourceType.Playlist)
				{
					string data = System.Text.Encoding.UTF8.GetString (args.SelectionData.Data);
					
					foreach (string d in data.Trim ().Split ('\n')) {
						
						Track track = Tracks.GetTrack (long.Parse (d));
						
						if (track == null)
							return;
							
						((PlaylistSource)source).AddTrack (track);
					}
					
					Saviour.Instance.Save ();
				}
				
				Drag.Finish (args.Context, true, false, args.Time);
				
			} catch (Exception e) {
				Tools.Log (this, 3, "{0}", e.Message);
			}
		}
	}
}
