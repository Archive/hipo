// EditableCellRenderer.cs
//
//  Copyright (C) 2008 Benoit Garret
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using Gtk;

namespace Hipo
{
	public enum Column
	{
		Icon,
		Name,
		Count,
		SourceType,
		Artist,
		Album,
		Title,
		Genre,
		Id
	}
	
	internal class EditableCellRenderer : CellRendererText
	{
		private Column column;
		
		public EditableCellRenderer ()
		{
			base.Editable = true;
			base.Ellipsize = Pango.EllipsizeMode.End;
		}
		
		public Column Column
		{
			get { return column; }
			set { column = value; }
		}
	}
}
