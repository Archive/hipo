// HipoMainWindow.cs
//
//  Copyright (C) 2008 Bneoit Garret
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using Gtk;
using Glade;
using NDesk.DBus;
using Mono.Unix;
using IPod;

namespace Hipo
{
	public class HipoMainWindow
	{
		[Widget] private Window hipoWindow;
		[Widget] private VBox menuBar;
		[Widget] private ScrolledWindow sourceListScroll;
		[Widget] private ScrolledWindow sourceScroll;
		[Widget] private Viewport defaultSourceWidget;
		[Widget] private HBox searchBox;
		[Widget] private Entry defaultSearchEntry;
		[Widget] private VBox sourceListBox;
		
		private SourceList sourceList;
		private PreferencesDialog preferencesDialog;
		private DeviceCombo combo;
		private Glade.XML gxml;
		private Gnome.Program program;
		private ImportDialog progressDialog;
		private CapacityBar capacityBar;
		
		private Widget sourceWidget;
		private Entry searchEntry;
		
		public HipoMainWindow (string[] args)
		{
			// disable logging
			Tools.LogLevel = 3;
			
			program = new Gnome.Program (Defines.PACKAGE, Defines.VERSION, Gnome.Modules.UI, args);
			
			// create the device combo and hook up device changed event
			Tools.Log (this, 1, "Creating the DeviceCombo");
			combo = new DeviceCombo ();
			combo.Changed += OnDeviceChanged;
			
			// Even though we don't (currently) use dbus, ipod-sharp does.
			// Without hooking up the glib main loop, some ipod-sharp functionality breaks.
			BusG.Init ();
			
			gxml = new Glade.XML (null, "hipo.glade", "hipoWindow", null);
			gxml.Autoconnect (this);
			
			// set window icon
			hipoWindow.Icon = Gnome.IconTheme.Default.LoadIcon ("gnome-dev-ipod", 16, 0);
			
			// hook up window delete event
			hipoWindow.DeleteEvent += OnDeleteEvent;
			
			// create action entries
			ActionEntry[] entries =  {

				new ActionEntry ("FileMenu",
									  null,
									  Catalog.GetString ("_File"),
									  null,
									  null,
									  null),
				new ActionEntry ("AddFile",
									  Stock.Add, Catalog.GetString ("_Add File..."),
									  null,
									  Catalog.GetString ("Add a file to your iPod"),
									  OnAddActivated),
				new ActionEntry ("AddFolder",
									  Stock.Add,
									  Catalog.GetString ("A_dd Folder..."),
									  "<control>O",
									  Catalog.GetString ("Add a folder to your iPod"),
									  OnAddFolder),
				new ActionEntry ("NewPlaylist",
									  Stock.New,
									  Catalog.GetString ("New Playlist"),
									  null,
									  Catalog.GetString ("Create a new Playlist"),
									  OnNewPlaylist),
				new ActionEntry ("Eject",
									  null,
									  Catalog.GetString ("_Eject"),
									  null,
									  null,
									  OnEjectActivated),
				new ActionEntry ("Quit",
									  Stock.Quit,
									  null,
									  "<control>Q",
									  null,
									  OnQuitActivated),
				
				new ActionEntry ("EditMenu",
									  null,
									  Catalog.GetString ("_Edit"),
									  null,
									  null,
									  null),
				new ActionEntry ("Preferences",
									  Stock.Preferences,
									  null,
									  null,
									  null,
									  OnPreferences),
				
				new ActionEntry ("HelpMenu",
									  null,
									  Catalog.GetString ("_Help"),
									  null,
									  null,
									  null),
				new ActionEntry ("Help",
									  Stock.Help,
									  null,
									  "F1",
									  null,
									  OnHelp),
				new ActionEntry ("About",
									  Gnome.Stock.About,
									  null,
									  null,
									  null,
									  OnAboutActivated)
			};
			
			// create the ui manager and the keyboard shortcuts
			ActionGroup group = new ActionGroup ("Hipo");
			group.Add (entries);
			
			UIManager.Instance.InsertActionGroup (group, 0);
			UIManager.Instance.AddUiFromResource ("menubar.xml");
			hipoWindow.AddAccelGroup (UIManager.Instance.AccelGroup);
			
			// create the menu
			menuBar.PackStart (UIManager.Instance.GetWidget ("/HipoMenu"), false, false, 0);
			menuBar.ReorderChild (UIManager.Instance.GetWidget ("/HipoMenu"), 0);
			
			// create the progress dialog
			progressDialog = new ImportDialog (hipoWindow);
			
			// create the preferences dialog
			preferencesDialog = new PreferencesDialog ();			
			
			// create the capacity bar
			capacityBar = new CapacityBar ();
			sourceListBox.PackStart (capacityBar, false, false, 0);
			
			// create the source list
			sourceList = SourceList.Instance;
			sourceListScroll.Child = sourceList.View;
			sourceListScroll.Child.ShowAll ();
			
			// the initial sourceWidget is the No device detected label
			sourceWidget = defaultSourceWidget;
			
			// ditto for the search entry
			searchEntry = defaultSearchEntry;
			
			// hook up the selection changed source list event
			sourceList.SelectionChanged += OnSourceListSelectionChanged;			
			
			Tools.Log (this, 3, "Finished creating the window");
			
			hipoWindow.ShowAll ();
			
			SetDevice ();
			
			program.Run ();
		}
		
		public Window Window
		{
			get {
				return hipoWindow;
			}
		}
		
		private void SetDevice (IPod.Device device)
		{
			capacityBar.Device = device;
			
			if (device == null)
			{
				// deactivate the menu options
				SetGreyedObjects (true);
				
				progressDialog.TrackDatabase = null;
				
				sourceList.SelectedSource = null;
				sourceList.Clear ();
				
				Tools.Log (this, 3, "No device set");
				
				return;
			}
			
			// activate the menu options
			SetGreyedObjects (false);
			
			progressDialog.TrackDatabase = device.TrackDatabase;
			
			// create the Saver instance
			Saviour.Instantiate (device);
			
			// create the device source
			sourceList.Add (new DeviceSource (device));
			
			// create tracks source and add it to the source list
			sourceList.Add (new TracksSource (device.TrackDatabase));
			
			// for each playlist in the track database create a playlist source and add it to the source list
			foreach (Playlist playlist in device.TrackDatabase.Playlists)
			{
				sourceList.Add (new PlaylistSource (playlist));
			}
			
			// select the tracks source
			sourceList.SelectedSource = SourceList.Instance.Tracks;
			
			sourceList.ExpandAll ();
			
			Tools.Log (this, 3, "Set device {0}", device.Name);
		}
		
		private void SetDevice ()
		{
			try {
				SetDevice (combo.ActiveDevice);
			} catch (Exception e) {
				Console.Error.WriteLine (e);
			}
		}
		
		private void SetGreyedObjects (bool greyed)
		{
			foreach (ActionGroup group in UIManager.Instance.ActionGroups)
			{
				if (group.Name == "Hipo")
				{
					group["AddFile"].Sensitive = !greyed;
					group["AddFolder"].Sensitive = !greyed;
					group["Eject"].Sensitive = !greyed;
					group["NewPlaylist"].Sensitive = !greyed;
				}
			}
		}
		
		private void OnAddActivated (System.Object o, EventArgs args)
		{
			string[] uris;
			
			FileFilter filter = new FileFilter ();
			
			FileChooserDialog fileChooser  = new FileChooserDialog (Catalog.GetString ("Select the file to add"),
											hipoWindow,
											FileChooserAction.Open,
											Stock.Cancel, ResponseType.Cancel,
											Stock.Add, ResponseType.Accept);
			
			fileChooser.SelectMultiple = true;
			fileChooser.LocalOnly = true;
			fileChooser.SetCurrentFolder (Settings.LastPath);
			
			filter.Name = "audio files";
			filter.AddMimeType ("audio/mpeg");
			filter.AddMimeType ("audio/x-m4a");
			fileChooser.AddFilter (filter);
			
			ResponseType res = (ResponseType)fileChooser.Run ();
			uris = fileChooser.Uris;
			Settings.LastPath = fileChooser.CurrentFolder;
			
			fileChooser.Destroy ();
			
			if (res == ResponseType.Accept)
			{
				sourceList.Tracks.AddTracks (uris, true);
			}
		}
		
		private void OnAddFolder (System.Object o, EventArgs args)
		{
			string[] uris;
			
			FileChooserDialog fileChooser = new FileChooserDialog (Catalog.GetString ("Select a folder to add"),
																					 hipoWindow,
																					 FileChooserAction.SelectFolder,
																					 Stock.Cancel, ResponseType.Cancel,
																					 Stock.Add, ResponseType.Accept);
			
			fileChooser.LocalOnly = true;
			fileChooser.SetCurrentFolder (Settings.LastPath);
			
			ResponseType res = (ResponseType) fileChooser.Run ();
			uris = fileChooser.Uris;
			Settings.LastPath = fileChooser.CurrentFolder;
			
			fileChooser.Destroy ();
			
			if (res == ResponseType.Accept)
			{
				sourceList.Tracks.AddFolder (uris, true);
			}
		}
		
		private void OnNewPlaylist (object o, EventArgs args)
		{
			HBox hbox = new HBox ();
			Label name = new Label (Catalog.GetString ("Name: "));
			Entry entry = new Entry ();
			
			Dialog dialog = new Dialog (Catalog.GetString ("New Playlist"), this.hipoWindow,
												 DialogFlags.DestroyWithParent,
												 Stock.Cancel, ResponseType.Cancel,
												 Stock.Ok, ResponseType.Accept);

			hbox.BorderWidth = 5;
			hbox.Add (name);
			hbox.Add (entry);
			hbox.ShowAll ();
			dialog.VBox.Add (hbox);

			ResponseType res = (ResponseType) dialog.Run ();
			dialog.Destroy ();
			
			if (res == ResponseType.Accept)
			{
				Playlist playlist = sourceList.Tracks.CreatePlaylist (entry.Text);
				sourceList.Add (new PlaylistSource (playlist));
			}
		}
		
		private void OnSourceListSelectionChanged (object o, SourceList.SelectionChangedArgs args)
		{
			sourceScroll.Remove (sourceWidget);
			searchBox.Remove (searchEntry);
			
			if (args.Source != null)
			{
				sourceWidget = args.Source.View;
				searchEntry = args.Source.SearchEntry;
				
			}
			else
			{
				sourceWidget = defaultSourceWidget;
				searchEntry = defaultSearchEntry;
			}
			
			sourceScroll.Child = sourceWidget;
			searchBox.PackStart (searchEntry, false, true, 0);
			sourceWidget.ShowAll ();
			searchEntry.ShowAll ();
		}
		
		private void OnDeviceChanged (object o, EventArgs args)
		{
			SetDevice (combo.ActiveDevice);
		}
		
		private void OnDeleteEvent (object o, DeleteEventArgs args)
		{
			OnQuitActivated (o, null);
		}
		
		private void OnQuitActivated (object o, EventArgs args)
		{
			Application.Quit ();
		}
		
		private void OnEjectActivated (object o, EventArgs args)
		{
			try {
				SourceList.Instance.Device.Eject ();
				Gtk.Application.Quit ();
			} catch (Exception e) {
				MessageDialog error = new MessageDialog (hipoWindow,
									 DialogFlags.DestroyWithParent,
									 MessageType.Error,
									 ButtonsType.Close,
									 Catalog.GetString ("There was an error ejecting the iPod:")
																	  );
				
				error.SecondaryText = e.Message;
				
				error.Run ();
				error.Destroy ();
			}
		}
		
		private void OnPreferences (object o, EventArgs args)
		{
			preferencesDialog.Show ();
		}
		
		private void OnHelp (object o, EventArgs args)
		{
			ShowHelp ("hipo.xml", null, hipoWindow.Screen, null);
		}
		
		// borrowed from tomboy
		private void ShowHelp (string filename, string link_id, Gdk.Screen screen, Window parent)
		{
			try {
				Gnome.Help.DisplayDesktopOnScreen (
								  Gnome.Program.Get (),
								  Defines.GNOME_HELP_DIR,
								  filename,
								  link_id,
								  screen);
			} catch {
				string message =
					Catalog.GetString ("The \"Hipo Manual\" could " +
											 "not be found.  Please verify " +
											 "that your installation has been " +
											 "completed successfully.");
				MessageDialog dialog = new MessageDialog (parent,
																		DialogFlags.DestroyWithParent,
																		MessageType.Error,
																		ButtonsType.Ok,
																		Catalog.GetString ("Help not found"),
																		message);
				dialog.Run ();
				dialog.Destroy ();
			}
		}
		
		private void OnAboutActivated (object o, EventArgs args)
		{
			AboutDialog aboutDialog = new AboutDialog ();
			string[] authors = { "Pedro Villavicencio Garrido <pvillavi@gnome.org>",
									  "Felipe Barros S. <felipeb@gnome.cl>",
									  "Benoit Garret <benoit.garret_gnome@gadz.org>" };
			string[] artists = { "Carlos Candia Flores <cerealk@vtr.net>",
									  "Gabriel Bustos Farret  <lemut@lemut.com>"	};
			
			string[] documenters = { "Gabriel Felipe Cornejo Salas <gfc@gnome.cl>",
											"Basilio Kublik <basilio@gnome.cl>" };
			
			aboutDialog.Authors = authors;
			aboutDialog.Copyright = "Copyright © 2006-2007 Pedro Villavicencio Garrido";
			aboutDialog.Name = Defines.PACKAGE;
			aboutDialog.Comments = Catalog.GetString ("iPod Management Tool");
			aboutDialog.Version = Defines.VERSION;
			aboutDialog.Artists = artists;
			aboutDialog.Documenters = documenters;
			aboutDialog.Logo = new Gdk.Pixbuf (null, "hipo-logo.png");
			
			aboutDialog.TranslatorCredits = Catalog.GetString ("translator-credits");
			
			aboutDialog.Website = "http://www.gnome.org/projects/hipo/";
			
			aboutDialog.Run ();
			aboutDialog.Destroy ();
		}
	}
}
