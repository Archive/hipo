// Tools.cs
//
//  Copyright (C) 2008 Benoit Garret
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;

namespace Hipo
{
	public static class Tools
	{
		private static int logLevel;
		
		public static string FormatString (double value)
		{
			if (value < 1073741824) {
				return String.Format ("{0:0.0} MB", (value / 1048576));
			} else {
				return String.Format ("{0:0.0} GB", (value / 1073741824));
			}
		}
		
		// TODO: a call for better logging (use the three levels, find out which messages are important and which ones are not)
		public static void Log (object o, int level, string message)
		{
			Log (o.ToString (), level, message);
		}
		
		public static void Log (object o, int level, string format, params object[] args)
		{
			Log (o, level, String.Format (format, args));
		}
		
		private static void Log (string header, int level, string message)
		{
			if (logLevel > 0)
			{
				if (level < 1)
					level = 1;
				if (level > 3)
					level = 3;
				
				if (logLevel >= level)
					Console.WriteLine ("{0}: {1}", header, message);
			}
		}
		
		public static int LogLevel
		{
			get {
				return logLevel;
			}
			
			set {
				logLevel = value;
			}
		}
	}
}
