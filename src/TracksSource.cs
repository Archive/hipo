// TracksSource.cs
//
//  Copyright (C) 2008 Benoit Garret
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using System.IO;
using System.Collections.Generic;
using Gtk;
using Gdk;
using IPod;
using Mono.Unix;

namespace Hipo
{
	public class TracksSource : TracksContainer
	{
		private TrackDatabase tracksDb;
		private string name;
		
		public TracksSource (TrackDatabase db) : base ()
		{
			this.tracksDb = db;
			Tracks = db.Tracks;
			
			this.name = Catalog.GetString ("Music");
			this.Icon = Gnome.IconTheme.Default.LoadIcon ("stock_music-library", 16, 0);
			
			this.Type = Hipo.SourceType.Tracks;
			this.Parent = SourceType.Device;
		}
		
		public override string Name
		{
			get {
				return name;
			}
			
			set {
			
			}
		}
		
		public override Menu Menu
		{
			get {
				return null;
			}
		}
		
		public override int Count
		{
			get {
				return tracksDb.Tracks.Count;
			}
		}
		
		public Playlist CreatePlaylist (string name)
		{
			Tools.Log (this, 2, "Creating playlist {0}", name);
			
			Playlist playlist = null;
			
			if ((playlist = tracksDb.CreatePlaylist (name)) != null)
			{
				Saviour.Instance.Save ();
			}
			
			return playlist;
		}
		
		public void DeletePlaylist (Playlist playlist)
		{
			Tools.Log (this, 2, "Removing playlist {0}", playlist.Name);
			tracksDb.RemovePlaylist (playlist);
		}
		
		public bool AddTrack (string path)
		{
			Tools.Log (this, 3, "Adding track {0} to the database", path);
			
			Track track = AddTrack (path, true);
			
			if (track == null)
				return false;
			else
				return true;
		}
		
		public override bool AddTracks (string[] uris, bool save)
		{
			Tools.Log (this, 2, "Adding {0} tracks to the database", uris.Length);
			
			bool trackAdded = false;
			
			foreach (string uri in uris)
			{
				Uri u = new Uri (uri);
				Track track = AddTrack (u.LocalPath, false);
				
				if (track == null)
					trackAdded |= false;
				else
				{
					trackAdded |= true;
				}
			}
			
			if (trackAdded && save)
				Saviour.Instance.Save ();
			
			return trackAdded;
		}
		
		public Track AddTrack (string path, bool save)
		{
			Track track = null;
			string ext = Path.GetExtension (path);

			if ((ext.ToLower () == ".mp3") || (ext.ToLower () == ".mp4") ||
			    (ext.ToLower () == ".m4a"))
			{
				TagLib.File file = TagLib.File.Create (path);
				
				lock (tracksDb)
					track = tracksDb.CreateTrack ();
				
				if ((file.Tag.FirstPerformer == null) || (file.Tag.FirstPerformer == String.Empty))
					track.Artist = "Unknown";
				else
					track.Artist = file.Tag.FirstPerformer;
				
				if ((file.Tag.Title == null) || (file.Tag.Title == String.Empty))
					track.Title = "Unknown";
				else
					track.Title = file.Tag.Title;
				
				if ((file.Tag.Album == null) || (file.Tag.Album == String.Empty))
					track.Album = "Unknown";
				else
					track.Album = file.Tag.Album;
				
				if ((file.Tag.FirstGenre == null) || (file.Tag.FirstGenre == String.Empty))
					track.Genre = "Unknown";
				else
					track.Genre = file.Tag.FirstGenre;
				
				track.FileName = path;
				track.Duration = file.Properties.Duration;
				track.TrackNumber = (int) file.Tag.Track;
				
				store.AppendValues (track);
				
				Tools.Log (this, 3, "Added {0}:{1}", track.Id, track.Title);
			}
			
			if (save && (track != null))
				Saviour.Instance.Save ();
			
			return track;
		}
		
		public override bool AddFolder (string[] dirs, bool save)
		{
			// returns true if at least one track has been added
			bool trackAdded = false;
			
			foreach (string path in dirs)
			{
				if (path == null)
					return false;
				
				System.Uri uri = new System.Uri (path);

				UnixDirectoryInfo info = new UnixDirectoryInfo (uri.LocalPath);
				foreach (UnixFileSystemInfo file in info.GetFileSystemEntries ())
				{
					// |= gives true as soon as one of the arguments is true
					if (!file.IsDirectory)
					{
						Track track = AddTrack (file.FullName, false);
						if (track == null)
							trackAdded |= false;
						else
							trackAdded |= true;
					}
					else
					{
						string[] tmp = { file.FullName };
						trackAdded |= AddFolder (tmp, false);
					}
				}
			}
			
			if (trackAdded && save)
				Saviour.Instance.Save ();

			return trackAdded;
		}
		
		public void RemoveTrack (Track track)
		{
			Tools.Log (this, 3, "Removing track {0} from the database", track.Title);
			RemoveTrack (track, true);
		}
		
		public override void RemoveTracks (List<Track> tracks)
		{
			Tools.Log (this, 2, "Removing {0} tracks from the database", tracks.Count);
			
			foreach (Track track in tracks)
			{
				RemoveTrack (track, false);
			}
			
			Saviour.Instance.Save ();
		}
		
		private void RemoveTrack (Track track, bool save)
		{
			tracksDb.RemoveTrack (track);
			store.Foreach (delegate (TreeModel model, TreePath path, TreeIter iter) {
				
				Track t = (Track)model.GetValue (iter, 0);
				
				if (track.Id == t.Id)
				{
					store.Remove (ref iter);
					Tools.Log (this, 3, "Removing track {0} from the database", track.Id);
					return true;
				}
				else
					return false;
			});
			
			if (save)
				Saviour.Instance.Save ();
		}
		
		public Track GetTrack (long id)
		{
			foreach (Track track in Tracks)
			{
				if (track.Id == id)
					return track;
			}
			
			return null;
		}
	}
}
