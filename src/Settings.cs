// Settings.cs
//
//  Copyright (C) 2008 Benoit Garret
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using System.Collections.Generic;
using Gtk;
using Glade;


namespace Hipo
{
	public class Settings
	{
		static GConf.Client client = new GConf.Client ();
		
		public static event GConf.NotifyEventHandler Changed
		{
			add {
				client.AddNotify ("/apps/hipo", value);
			}
			remove{
				client.RemoveNotify ("/apps/hipo", value);
			}
		}
		
		public static Array TracksviewColumns
		{
			get {
				
				try {
					return (Array) client.Get (SettingKeys.TracksviewColumns);
				}
				catch (GConf.NoSuchKeyException) {
					
					string[] columns = {"Artist", "Album", "Title", "Genre"};
					
					client.Set (SettingKeys.TracksviewColumns, (object) columns);
					return columns;
				}
			}
			set {
				// UGLY HACK: gconf-sharp does not seem to accept empty arrays
				Array v = value;
				
				if (v.Length > 0)
					client.Set (SettingKeys.TracksviewColumns, value);
				else
				{
					Array empty = Array.CreateInstance (typeof (string), 1);
					empty.SetValue ("", 0);
					client.Set (SettingKeys.TracksviewColumns, empty);
				}
			}
		}
		
		public static event GConf.NotifyEventHandler TracksviewColumnsChanged
		{
			add {
				client.AddNotify (SettingKeys.TracksviewColumns, value);
			}
			remove{
				client.RemoveNotify (SettingKeys.TracksviewColumns, value);
			}
		}
		
		public static string LastPath
		{
			get {
				
				string path;
				
				try {
					path = (string) client.Get (SettingKeys.LastPath);
				}
				catch (GConf.NoSuchKeyException) {
					
					path = Environment.GetFolderPath (Environment.SpecialFolder.Desktop);
					
					client.Set (SettingKeys.LastPath, (object) path);
				}
				
				return path;
			}
			set {
				// always have a valid directory set
				if (value != null)
					client.Set (SettingKeys.LastPath, value);
				else
					client.Set (SettingKeys.LastPath,
					            Environment.GetFolderPath (Environment.SpecialFolder.Desktop));
			}
		}

		public static event GConf.NotifyEventHandler LastPathChanged
		{
			add {
				client.AddNotify (SettingKeys.LastPath, value);
			}
			remove{
				client.RemoveNotify (SettingKeys.LastPath, value);
			}
		}
	}

	public class SettingKeys
	{
		public static string LastPath
		{
			get {
				 return "/apps/hipo/last_path";
			}
		}
		
		public static string TracksviewColumns
		{
			get {
				return "/apps/hipo/tracksview_columns";
			}
		}
		
	}
	
	public class PreferencesDialog
	{
		[Widget] private Window properties;
		[Widget] private CheckButton checkbutton_artist;
		[Widget] private CheckButton checkbutton_album;
		[Widget] private CheckButton checkbutton_title;
		[Widget] private CheckButton checkbutton_genre;
		[Widget] private Button button_close;
		
		private Glade.XML gxml;
		
		public PreferencesDialog ()
		{
			Tools.Log (this, 3, "Creating the PreferencesDialog");
			
			gxml = new Glade.XML (null, "hipo.glade", "properties", null);
			gxml.Autoconnect (this);
			
			Array columns = Settings.TracksviewColumns;
			
			foreach (string column in columns)
			{
				if (column == "Album")
					checkbutton_album.Active = true;
				if (column == "Artist")
					checkbutton_artist.Active = true;
				if (column == "Title")
					checkbutton_title.Active = true;
				if (column == "Genre")
					checkbutton_genre.Active = true;
			}
			
			checkbutton_album.Toggled += OnButtonToggled;
			checkbutton_artist.Toggled += OnButtonToggled;
			checkbutton_title.Toggled += OnButtonToggled;
			checkbutton_genre.Toggled += OnButtonToggled;
			button_close.Clicked += OnCloseClicked;
		}
		
		public void Show ()
		{
			properties.ShowAll ();
		}
		
		private void OnButtonToggled (object o, EventArgs args)
		{
			List<string> columns = new List<string> ();
			
			if (checkbutton_artist.Active)
				columns.Add ("Artist");
			if (checkbutton_album.Active)
				columns.Add ("Album");
			if (checkbutton_title.Active)
				columns.Add ("Title");
			if (checkbutton_genre.Active)
				columns.Add ("Genre");
			
			Settings.TracksviewColumns = columns.ToArray ();
		}
		
		private void OnCloseClicked (object o, EventArgs args)
		{
			properties.HideAll ();
		}
	}
}
