// PlaylistSource.cs
//
//  Copyright (C) 2008 Benoit Garret
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using System.Collections.Generic;
using Gtk;
using IPod;
using Mono.Unix;

namespace Hipo
{
	public class PlaylistSource : TracksContainer
	{
		Playlist playlist;
		Menu menu;
		public override event SourceDeletedEventHandler Deleted;
		
		public PlaylistSource (Playlist playlist) : base ()
		{
			Tools.Log (this, 1, "Creating playlist source for {0}", playlist.Name);
			
			this.playlist = playlist;
			Tracks = playlist.Tracks;
			
			this.Type = Hipo.SourceType.Playlist;
			this.Parent = SourceType.Device;
			this.Icon = Gnome.IconTheme.Default.LoadIcon ("stock_playlist", 16, 0);
			
			// hook up add and remove track event
			playlist.TrackAdded += OnTrackAdded;
			playlist.TrackRemoved += OnTrackRemoved;
			
			// menu creation
			menu = new Menu();
			
			MenuItem item = (MenuItem)(new Action ("RenamePlaylist",
			                                       Catalog.GetString ("Rename"),
			                                       Catalog.GetString ("Rename the playlist"),
			                                       Gtk.Stock.Copy)).CreateMenuItem ();
			item.Activated += OnRename;
			menu.Add (item);
			
			item = (MenuItem)(new Action ("DeletePlaylist",
			                              Catalog.GetString ("Delete"),
			                              Catalog.GetString ("Delete the playlist"),
			                              Gtk.Stock.Delete)).CreateMenuItem ();
			item.Activated += OnDelete;
			menu.Add (item);
		}
		
		public override string Name
		{
			get {
				return playlist.Name;
			}
			set {
				if (value != playlist.Name)
				{
					playlist.Name = value;
					Saviour.Instance.Save ();
				}
			}
		}
		
		public override Menu Menu
		{
			get {
				return menu;
			}
		}
		
		public override int Count
		{
			get {
				return playlist.Tracks.Count;
			}
		}
		
		public void AddTrack (Track track)
		{
			Tools.Log (this, 3, "Adding track {0} to the playlist {1}", track.Title, playlist.Name);
			AddTrack (track, true);
		}
		
		public void AddTracks (List<Track> tracks)
		{
			foreach (Track track in tracks)
			{
				AddTrack (track, false);
			}
			
			Saviour.Instance.Save ();
		}
		
		private void AddTrack (Track track, bool save)
		{
			playlist.AddTrack (track);
		}
		
		// Adds the tracks to the database, then to the playlist
		public override bool AddTracks (string[] uris, bool save)
		{
			Tools.Log (this, 2, "Adding {0} tracks to the playlist {1}", uris.Length, playlist.Name);
			
			bool trackAdded = false;
			
			foreach (string uri in uris)
			{
				Uri u = new Uri (uri);
				Track track = SourceList.Instance.Tracks.AddTrack (u.LocalPath, false);
				
				if (track == null)
					trackAdded |= false;
				else
				{
					trackAdded |= true;
					AddTrack (track, false);
				}
			}
			
			if (trackAdded && save)
				Saviour.Instance.Save ();
			
			return trackAdded;
		}
		
		// Adds the tracks to the database, then to the playlist
		public override bool AddFolder (string[] dirs, bool save)
		{
			// returns true if at least one track has been added
			bool trackAdded = false;
			
			foreach (string path in dirs)
			{
				if (path == null)
					return false;

				System.Uri uri = new System.Uri (path);

				UnixDirectoryInfo info = new UnixDirectoryInfo (uri.LocalPath);
				foreach (UnixFileSystemInfo file in info.GetFileSystemEntries ())
				{
					// |= gives true as soon as one of the arguments is true
					if (!file.IsDirectory)
					{
						Track track = SourceList.Instance.Tracks.AddTrack (file.FullName, false);
					
						if (track == null)
							trackAdded |= false;
						else
						{
							trackAdded |= true;
							AddTrack (track, false);
						}
					}
					else
					{
						string[] tmp = { file.FullName };
						trackAdded |= AddFolder (tmp, false);
					}
				}
			}
			
			if (trackAdded && save)
				Saviour.Instance.Save ();

			return trackAdded;
		}
		
		public void RemoveTrack (Track track)
		{
			Tools.Log (this, 3, "Removing track {0} from the playlist {1}", track.Title, playlist.Name);
			RemoveTrack (track, true);
		}
		
		public override void RemoveTracks (List<Track> tracks)
		{
			Tools.Log (this, 2, "Removing {0} tracks from the playlist {1}", tracks.Count, playlist.Name);
			
			foreach (Track track in tracks)
			{
				RemoveTrack (track, false);
			}
			
			Saviour.Instance.Save ();
		}
		
		private void RemoveTrack (Track track, bool save)
		{
			playlist.RemoveTrack (track);
			
			if (save)
				Saviour.Instance.Save ();
		}
		
		private void OnTrackAdded (object o, int index, Track track)
		{
			Tools.Log (this, 3, "Adding track {0} to playlist {1} at position {2}", track.Title, this.Name, index);
			store.AppendValues (track);
		}
		
		private void OnTrackRemoved (object o, int index, Track track)
		{
			TreePath path = new TreePath ();
            path.AppendIndex (index);

            TreeIter iter = TreeIter.Zero;

            if (!store.GetIter (out iter, path))
                return;
            
            store.Remove (ref iter);
		}
		
		private void OnRename (object o, EventArgs args)
		{
			Dialog dialog = new Dialog (Catalog.GetString ("Rename the playlist"), null, Gtk.DialogFlags.DestroyWithParent);
			
			dialog.Modal = true;
			dialog.AddButton ("Cancel", ResponseType.Close);
			dialog.AddButton ("OK", ResponseType.Ok);
			
			Gtk.Entry entry = new Gtk.Entry (this.Name);
			dialog.VBox.PackStart (entry, false, false, 5);
			dialog.DefaultResponse = ResponseType.Ok;
			
			dialog.ShowAll ();
			ResponseType r = (ResponseType) dialog.Run ();
			
			if (r == ResponseType.Ok)
			{
				this.Name = entry.Text;
			}
			
			dialog.Destroy ();
		}
		
		private void OnDelete (object o, EventArgs args)
		{
			Tools.Log (this, 2, "Deleting playlist {0}", playlist.Name);
			
			SourceList.Instance.Tracks.DeletePlaylist (playlist);
			playlist = null;
			
			if (Deleted != null)
			{
				Deleted (this);
			}
			
			Saviour.Instance.Save ();
		}
	}
}
