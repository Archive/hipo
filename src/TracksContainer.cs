// TracksContainer.cs
//
//  Copyright (C) 2008 Benoit Garret
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using System.IO;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Gtk;
using Gdk;
using Glade;
using GConf;
using IPod;
using Mono.Unix;

namespace Hipo
{
	// this class contains the shared code for all sources containing tracks
	
	public abstract class TracksContainer  : SourceBase
	{
		private TreeView view;
		protected ListStore store;
		private TreeModelFilter store_filter;
		private Entry search;
		private SourceType type;
		private IList<Track> tracks;
		private Column sortColumn = Column.Artist;
		private MenuItem addToPlaylistItem;
		private Menu menu;
		
		private static TargetEntry [] source_entries = {
			new TargetEntry ("text/uri-list", 0, 0),
			new TargetEntry ("application/x-hipo", 0, 0)
		};
		
		public TracksContainer ()
		{
			Tools.Log (this, 3, "Creating tracks container");
			
			// create the columns, treeview, etc
			view = new TreeView ();
			
			BuildColumns (Settings.TracksviewColumns);
			
			view.HeadersVisible = true;
			view.HeadersClickable = true;
			
			// create the store
			store = new ListStore (typeof (Track));
			store.SetSortFunc (0, CompareFunc);
			
			// bind store and view
			view.Model = store;
			
			// enable drag and drop
			view.EnableModelDragSource (Gdk.ModifierType.Button1Mask,
			                            source_entries, Gdk.DragAction.Copy);
			view.DragDataGet += OnDragDataGet;

			view.EnableModelDragDest (source_entries, Gdk.DragAction.Copy);
			view.DragDataReceived += OnDragDataReceived;
			
			view.Selection.Mode = SelectionMode.Multiple;
			
			// hook up popup menu handlers
			view.KeyPressEvent += OnKeyPress;
			view.ButtonPressEvent += OnButtonPress;
			
			// hook up the change column settings event
			Hipo.Settings.TracksviewColumnsChanged += OnColumnsChanged;
			
			// create the search entry and bind the changed event
			search = new Entry ();
			search.Changed += SearchFor;
			store_filter = new TreeModelFilter (view.Model, null);
			store_filter.VisibleFunc = SearchFilterFunc;
			
			// menu creation
			menu = new Menu();
			
			addToPlaylistItem = (MenuItem)(new Action ("AddToPlaylist",
			                                           Catalog.GetString ("Add to Playlist..."),
			                                           Catalog.GetString ("Add the track to a Playlist"),
			                                           Gtk.Stock.Add)).CreateMenuItem ();
			menu.Add (addToPlaylistItem);
			
			MenuItem item = (MenuItem)(new Action ("RemoveTrack",
			                                       Catalog.GetString ("Remove"),
			                                       Catalog.GetString ("Remove the track from your iPod"),
			                                       Gtk.Stock.Remove)).CreateMenuItem ();
			item.Activated += OnRemove;
			menu.Add (item);
			
			item = (MenuItem)(new Action ("TrackProperties",
			                              Catalog.GetString ("Track Properties"),
			                              Catalog.GetString ("Track Properties"),
			                              Gtk.Stock.Properties)).CreateMenuItem ();
			item.Activated += OnTrackProperties;
			menu.Add (item);
		}
		
		private void BuildColumns (Array wanted_columns)
		{
			Array all_columns = Enum.GetNames (typeof (Column));
			
			foreach (string wanted_column in wanted_columns)
			{
				foreach (string column in all_columns)
				{
					if (column == wanted_column)
					{
						Gtk.TreeViewColumn col = BuildColumn ((Column) Enum.Parse (typeof (Column), column));
						view.AppendColumn (col);
					}
				}
			}
		}
		
		private TreeViewColumn BuildColumn (Column column)
		{
			EditableCellRenderer cell = new EditableCellRenderer ();
			cell.Edited += OnTrackEdit;
			cell.Column = column;
			
			TreeViewColumn col = new TreeViewColumn ();
			col.Title = Catalog.GetString (column.ToString ());
			col.PackStart (cell, true);
			
			TreeCellDataFunc textCellDataFunc = new TreeCellDataFunc (TextCellDataFunc);
			col.SetCellDataFunc (cell, textCellDataFunc);
			
			col.Clickable = true;
			col.Clicked += delegate { sortColumn = column; };
			
			col.Resizable = true;
			col.Expand = false;
			
			col.SortIndicator = true;
			col.SortColumnId = 0;
			
			col.Sizing = TreeViewColumnSizing.Fixed;
			
			if (column == Column.Genre)
				col.FixedWidth = 100;
			else
				col.FixedWidth = 200;
			
			return col;
		}
		
		private void TextCellDataFunc (TreeViewColumn column, CellRenderer renderer,
		                               TreeModel model, TreeIter iter)
		{
			Track track = (Track) model.GetValue (iter, 0);
			EditableCellRenderer cell = (EditableCellRenderer)renderer;
			
			switch (cell.Column)
			{
			case Column.Album:
				cell.Text = track.Album;
				break;
			case Column.Artist:
				cell.Text = track.Artist;
				break;
			case Column.Genre:
				cell.Text = track.Genre;
				break;
			case Column.Title:
				cell.Text = track.Title;
				break;
			default:
				cell.Text = "";
				break;
			}
		}
		
		int CompareFunc (TreeModel model, TreeIter a, TreeIter b)
		{
			Track tracka = (Track)store.GetValue (a, 0);
			Track trackb = (Track)store.GetValue (b, 0);
			
			switch (sortColumn)
			{
			case Column.Album:
				return String.Compare (tracka.Album, trackb.Album);
			case Column.Artist:
				return String.Compare (tracka.Artist, trackb.Artist);
			case Column.Genre:
				return String.Compare (tracka.Genre, trackb.Genre);
			case Column.Title:
				return String.Compare (tracka.Title, trackb.Title);
			default:
				return 0;
			}
		}
		
		public override Widget View
		{
			get {
				return (Widget)view;
			}
		}

		public override Entry SearchEntry
		{
			get {
				return search;
			}
		}

		public override SourceType Type
		{
			get {
				return type;
			}
			
			protected set {
				this.type = value;
			}
		}
		
		protected IList<Track> Tracks
		{
			set {
				tracks = value;
				store.Clear ();
				
				foreach (Track track in tracks)
				{
					Tools.Log (this, 3, "Added {0}:{1}:{2}", track.Id, track.Title, track.FileName);
					store.AppendValues (track);
				}
			}
			
			get {
				return tracks;
			}
		}
		
		// the two following methods have to be implemented
		// to add tracks to the source when drag and dropped to.
		public virtual bool AddTracks (string[] paths, bool save)
		{
			return true;
		}
		
		public virtual bool AddFolder (string[] dirs, bool save)
		{
			return true;
		}
		
		// implement this to make the right click menu delete item work
		public virtual void RemoveTracks (List<Track> tracks)
		{
			
		}
		
		private void OnColumnsChanged (object o, NotifyEventArgs args)
		{
			foreach (TreeViewColumn column in view.Columns)
			{
				view.RemoveColumn (column);
			}
			
			Array wanted_columns = (Array)args.Value;
			
			BuildColumns (wanted_columns);
		}
		
		[GLib.ConnectBefore]
		private void OnButtonPress (object o, ButtonPressEventArgs args)
		{
			// if right click then show the menu
			switch (args.Event.Button)
			{
			case 3:
				Tools.Log (this, 3, "Ooooh, right click");
				PopupMenu (args.Event);
				args.RetVal = true;
				break;
			}
		}
		
		private void OnKeyPress (object o, KeyPressEventArgs args)
		{
			switch (args.Event.Key)
			{
			case Gdk.Key.Menu:
				Tools.Log (this, 3, "Ooooh, menu button");
				PopupMenu (args.Event);
				args.RetVal = true;
				break;
			case Gdk.Key.Delete:
				Tools.Log (this, 3, "Ooooh, delete button");
				RemoveTracks (GetSelectedTracks ());
				args.RetVal = true;
				break;
			}
		}
		
		private void PopupMenu (Gdk.Event ev)
		{
			// build the menu and show it
			
			if (view.Selection.CountSelectedRows () > 0)
			{
				Tools.Log (this, 3, "Building and showing track popup menu");
				
				Menu playlistMenu = new Menu ();
				
				foreach (ISource source in SourceList.Instance.Playlists)
				{
					PlaylistSource playlistSource = (PlaylistSource)source;
					ImageMenuItem item = new ImageMenuItem (playlistSource.Name);
					item.Image = new Gtk.Image (Gnome.IconTheme.Default.LoadIcon ("stock_playlist", 16, 0));
					// eww, ugly 
					item.Activated += delegate {
						
						AddToPlaylist (playlistSource);
					};
					playlistMenu.Append (item);
					Tools.Log (this, 3, "Added playlist {0} to the menu", playlistSource.Name);
				}
				
				addToPlaylistItem.Submenu = playlistMenu;
				
				menu.ShowAll ();
				playlistMenu.ShowAll ();
				
				menu.Popup (null, null, null, ((ev != null) && ev.Type == Gdk.EventType.ButtonPress) ? ((Gdk.EventButton)ev).Button : 0,
				            Gdk.EventHelper.GetTime (ev));
			}
		}
		
		private void AddToPlaylist (PlaylistSource playlistSource)
		{
			playlistSource.AddTracks (GetSelectedTracks ());
		}
		
		private List<Track> GetSelectedTracks ()
		{
			TreeIter iter;
			TreeModel model;
			
			Array treePaths = view.Selection.GetSelectedRows (out model);
			List<Track> selectedTracks = new List<Track>();
			
			foreach (TreePath tPath in treePaths)
			{
				if (model.GetIter (out iter, tPath))
				{
					Track track = (Track)model.GetValue (iter, 0);
					selectedTracks.Add (track);
				}
			}
			
			return selectedTracks;
		}
		
		private void OnTrackEdit (object o, EditedArgs args)
		{
			TreeIter iter = TreeIter.Zero;
			EditableCellRenderer cell = (EditableCellRenderer) o;
			bool modified = false;
			
			if (!store.GetIter (out iter, new TreePath (args.Path)))
				return;
			
			Track track = (Track)store.GetValue (iter, 0);
			
			switch (cell.Column)
			{
				case Column.Artist:
					if (args.NewText != track.Artist)
					{
						track.Artist = args.NewText;
						modified = true;
					}
					break;
				case Column.Album:
					if (args.NewText != track.Album)
					{
						track.Album = args.NewText;
						modified = true;
					}
					break;
				case Column.Title:
					if (args.NewText != track.Title)
					{
						track.Title = args.NewText;
						modified = true;
					}
					break;
				case Column.Genre:
					if (args.NewText != track.Genre)
					{
						track.Genre = args.NewText;
						modified = true;
					}
					break;
				default:
					break;
			}
			
			if (modified)
			{
				Saviour.Instance.Save ();
			}
		}
		
		[Widget] private Gtk.Dialog trackProperties;
		[Widget] private Gtk.Entry artistEntry;
		[Widget] private Gtk.Entry albumEntry;
		[Widget] private Gtk.Entry titleEntry;
		[Widget] private Gtk.Entry genreEntry;
		[Widget] private Gtk.SpinButton numberSpin;
		[Widget] private Gtk.Image artwork;
		[Widget] private Gtk.Button openArtwork;
		[Widget] private Gtk.Label titleLabel;
		[Widget] private Gtk.Label numberLabel;
                
		private void OnTrackProperties (object o, EventArgs args)
		{
			Glade.XML xml = new Glade.XML (null, "hipo.glade", "trackProperties", null);
			xml.Autoconnect (this);

			bool modified = false;
			Gdk.Pixbuf newCover = null;
			string artist = "";
			string album = "";
			string title = "";
			string genre = "";
			int trackNumber = 0;
			Gdk.Pixbuf image = null;
			
			List<Track> selectedTracks = GetSelectedTracks ();
			
			if (selectedTracks.Count > 0)
			{
				foreach (Track track in selectedTracks)
				{
					/*
					The values are filled in with the properties of the first track of the list.
					A comparison is made with the subsequent tracks,
					if the values are the same, nothing is changed,
					if they are different, a blank value is put
					*/
					
					if (selectedTracks.IndexOf (track) == 0)
					{
						artist = track.Artist;
						album = track.Album;
						title = track.Title;
						genre = track.Genre;
						trackNumber = track.TrackNumber;
						
						foreach (ArtworkFormat format in SourceList.Instance.Device.LookupArtworkFormats (ArtworkUsage.Cover)) {

							if (format != null) {
								if (track.HasCoverArt (format)) 
									image = ArtworkHelpers.ToPixbuf (format, track.GetCoverArt (format));
							}
						}
						
						if (selectedTracks.Count == 1)
						{
							/* artist name and track title properties */
							trackProperties.Title = String.Format (Catalog.GetString ("{0} {1} Properties"),
											       track.Artist, track.Title);
						}
						else
						{
							trackProperties.Title = Catalog.GetString ("Multiple tracks properties");
                                                        titleEntry.Hide ();
                                                        titleLabel.Hide ();
                                                        numberLabel.Hide ();
                                                        numberSpin.Hide ();
						}
					}
					else
					{
						if (!artist.Equals("") && !artist.Equals (track.Artist))
							artist = "";
						
						if (!album.Equals("") && !album.Equals (track.Album))
							album = "";
						
						if (!title.Equals("") && !title.Equals (track.Title))
							title = "";
						
						if (!genre.Equals("") && !genre.Equals (track.Genre))
							genre = "";
						
						if ((trackNumber != 0) && (trackNumber != track.TrackNumber))
							trackNumber = 0;
						
					}
				}
				
				artistEntry.Text = artist;
				albumEntry.Text = album;
				titleEntry.Text = title;
				genreEntry.Text = genre;
				numberSpin.Value = trackNumber;
				artwork.FromPixbuf = image;
			}

			openArtwork.Clicked += delegate {

				FileFilter filter = new Gtk.FileFilter ();
				
				Gtk.FileChooserDialog fileChooser  = new Gtk.FileChooserDialog (Catalog.GetString ("Select an image"),
												trackProperties,
												FileChooserAction.Open,
												Gtk.Stock.Cancel, ResponseType.Cancel,
												Gtk.Stock.Add, ResponseType.Accept);
				
				fileChooser.LocalOnly = true;
				filter.AddMimeType ("image/png");
				filter.AddMimeType ("image/jpeg");
				fileChooser.AddFilter (filter);
				fileChooser.UpdatePreview += OnUpdatePreview;
				fileChooser.PreviewWidget = new Gtk.Image ();
				
				ResponseType res = (ResponseType) fileChooser.Run ();
				string uri = fileChooser.Uri;
				
				if (res == ResponseType.Accept)
				{
					Uri ur = new Uri (uri);
					newCover = new Gdk.Pixbuf (ur.LocalPath);

					artwork.FromPixbuf = newCover.ScaleSimple(200, 200, Gdk.InterpType.Bilinear);
				}

				fileChooser.Destroy ();
			};

			ResponseType response = (ResponseType) trackProperties.Run ();
				
			if (response == ResponseType.Ok)
			{
				if (artistEntry.Text != artist) {
					modified = true;
					foreach (Track track in selectedTracks)
					{
						track.Artist = artistEntry.Text;
					}
				}

				if (albumEntry.Text != album) {
					modified = true;
					foreach (Track track in selectedTracks)
					{
						track.Album = albumEntry.Text;
					}
				}

				if (titleEntry.Text != title) {
					modified = true;
					foreach (Track track in selectedTracks)
					{
						track.Title = titleEntry.Text;
					}
				}

				if (genreEntry.Text != genre) {
					modified = true;
					foreach (Track track in selectedTracks)
					{
						track.Genre = genreEntry.Text;
					}
				}

				if (numberSpin.Value != trackNumber) {
					modified = true;
					foreach (Track track in selectedTracks)
					{
						track.TrackNumber = (int) numberSpin.Value;
					}
				}

				if (newCover != null) {
					
					foreach (ArtworkFormat format in SourceList.Instance.Device.LookupArtworkFormats (ArtworkUsage.Cover)) {

						// maybe is a good idea to set all the album of the current selected track with the new cover.
						// we can set this on a gconf key.
						
						foreach (Track track in selectedTracks)
						{
							track.SetCoverArt (format, ArtworkHelpers.ToBytes (format, newCover));
						}
					}
					
					modified = true;
				}
			}

			if (modified)
				Saviour.Instance.Save ();

			trackProperties.Destroy ();
		}
		
		private void OnUpdatePreview (object o, EventArgs args)
		{
			FileChooser fileChooser = (FileChooser)o;
			
			try {
				
				if (fileChooser.PreviewFilename != null)
				{
					Gdk.Pixbuf cover = new Gdk.Pixbuf (fileChooser.PreviewFilename);
					
					((Gtk.Image)fileChooser.PreviewWidget).Pixbuf = cover.ScaleSimple (150, 150, Gdk.InterpType.Bilinear);
					
					((Gtk.Image)fileChooser.PreviewWidget).Show ();	
				}
			} catch (Exception e) {
				
				((Gtk.Image)fileChooser.PreviewWidget).Hide ();
				Console.WriteLine (e.Message);
			}
		}
		
		private void OnRemove (object o, EventArgs args)
		{
			RemoveTracks (GetSelectedTracks ());
		}
		
		private void OnDragDataGet (object o, DragDataGetArgs args)
		{
			Atom[] targets = args.Context.Targets; 
			StringBuilder dragData = new StringBuilder ();
			
			List<Track> selectedTracks = GetSelectedTracks ();
			
			foreach (Track track in selectedTracks)
			{
				if (args.SelectionData.Target == "text/uri-list")
					dragData.Append (track.FileName + "\r\n");
				else
					dragData.Append (track.Id + "\n");
			}
			
			args.SelectionData.Set (targets[0], 8,
			                        System.Text.Encoding.UTF8.GetBytes (dragData.ToString()));
		}
		
		private void OnDragDataReceived (object o, DragDataReceivedArgs args)
		{
			try {
				
				if ((Gtk.Drag.GetSourceWidget (args.Context) != view) &&
				    (args.SelectionData.Length >=0 && args.SelectionData.Format == 8))
				{
					string data = System.Text.Encoding.UTF8.GetString (args.SelectionData.Data);
					bool trackAdded = false;
					
					foreach (string ur in data.Trim ().Split ('\n'))
					{
						string formatUri = ur.Trim ();
						
						Uri uri = new Uri (formatUri);
						
						if (Directory.Exists (uri.LocalPath))
						{
							string[] tmp = { formatUri };
							trackAdded |= AddFolder (tmp, false);
						}
						else if (File.Exists (uri.LocalPath))
						{
							string[] tmp = { formatUri };
							trackAdded |= AddTracks (tmp, false);
						}
					}
					
					if (trackAdded)
						Saviour.Instance.Save ();
					
					Gtk.Drag.Finish (args.Context, true, false, args.Time);
				}
			} catch (Exception e) {
				Console.WriteLine (e.Message);
			}
		}
		
		private bool SearchFilterFunc (TreeModel model, TreeIter iter)
		{
			if (search.Text.Trim().Length < 1)
				return true;
			
			bool ret = true;
			Track track = (Track)model.GetValue (iter, 0);
			
			try {
				string search_term = search.Text.Trim().ToLower();
				string artist = track.Artist.ToLower();
				string album = track.Album.ToLower();
				string title = track.Title.ToLower();
				string genre = track.Genre.ToLower();
				ret = ( artist.Contains(search_term) ||
					album.Contains(search_term) ||
					title.Contains(search_term) ||
					genre.Contains(search_term)
					);
			} catch (Exception e) {
				Console.WriteLine (e.Message);
			}
			return ret;
		}
		
		private void SearchFor (object o, EventArgs args)
		{
			view.Model = store_filter;
			store_filter.Refilter();
		}
	}
}
