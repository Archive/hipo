// Saver.cs
//
//  Copyright (C) 2008 Benoit Garret
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using System.Threading;
using IPod;

namespace Hipo
{
	// handles the save operations
	public class Saviour
	{
		private static Saviour saviour = null;
		private Device device;
		
		public Saviour (Device device)
		{
			Tools.Log (this, 2, "A saviour named {0} is born", device.Name);
			this.device = device;
		}
		
		public static Saviour Instance
		{
			get {
				return saviour;
			}
		}
		
		public static void Instantiate (Device device)
		{
			saviour = new Saviour (device);
		}
		
		public void Save ()
		{
			Tools.Log (this, 3, "Saving database");
			
			ThreadStart starter = delegate {
				
				lock (this.device) {
					this.device.Save ();
				}
			};
			
			new Thread(starter).Start();
		}
	}
	
	public class UIManager : Gtk.UIManager
	{
		private static Gtk.UIManager manager = null;
		
		public UIManager () : base ()
		{
			Tools.Log (this, 3, "Creating a new UIManager");
		}
		
		public static Gtk.UIManager Instance {
			
			get {
				
				if (manager == null)
				{
					manager = new UIManager ();
				}
				
				return manager;
			}
		}
	}
}
