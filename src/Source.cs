// Source.cs
//
//  Copyright (C) 2008 Benoit Garret
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//

using System;
using System.Collections.Generic;
using Gtk;
using Gdk;

namespace Hipo
{	
	public delegate void SourceDeletedEventHandler (object source);
	
	public interface ISource
	{
		event SourceDeletedEventHandler Deleted;
		
		// name displayed in the source list
		string Name {get; set;}
		
		// widget providing a view of the source (goes in the right panel)
		Widget View {get;}
		
		// search entry filtering tracks, photos, etc
		Entry SearchEntry {get;}
		
		// menu shown when right clicked in the source list
		Menu Menu {get;}
		
		// source type
		SourceType Type {get;}
		
		// source icon
		Pixbuf Icon {get;}
		
		// source item count
		int Count {get;}
		
		// source id
		int Id {get;}
		
		// source parent
		SourceType Parent {get;}
	}
	
	public abstract class SourceBase : ISource
	{
		private static int idCounter = 0;
		private int id;
		private string name;
		private Viewport view;
		private Entry searchEntry;
		private Menu menu;
		private SourceType type;
		private Pixbuf icon;
		private int count;
		private SourceType parent;
		
		public virtual event SourceDeletedEventHandler Deleted;
		
		public SourceBase ()
		{
			id = idCounter;
			idCounter++;
			
			Tools.Log (this, 3, "Creating source {0}", id);
			
			// initialize everything with dummy values
			view = new Viewport ();
			searchEntry = new Entry ();
			searchEntry.Sensitive = false;
			menu = new Menu ();
			type = SourceType.None;
			count = 0;
			name = "Default name, override me";
		}
		
		public virtual string Name
		{
			get {
				return name;
			}
			
			set {
				name = value;
			}
		}
		
		public virtual Widget View
		{
			get {
				return view;
			}
		}
		
		public virtual Entry SearchEntry
		{
			get {
				return searchEntry;
			}
		}
		
		public virtual Menu Menu
		{
			get {
				return menu;
			}
		}
		
		public virtual SourceType Type
		{
			get {
				return type;
			}
			
			protected set {
				type = value;
			}
		}
		
		public virtual Pixbuf Icon
		{
			get {
				return icon;
			}
			
			protected set {
				icon = value;
			}
		}
		
		public virtual SourceType Parent
		{
			get {
				return parent;
			}
			
			protected set {
				parent = value;
			}
		}
		
		public virtual int Count
		{
			get {
				return count;
			}
		}
		
		public int Id
		{
			get {
				return id;
			}
		}
	}
	
	public enum SourceType
	{
		None,
		Device,
		Tracks,
		Playlist,
		SmartPlaylist
	}
}
