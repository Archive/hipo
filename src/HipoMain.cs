using System;
using System.IO;
using Mono.Unix;
using Gtk;
using Glade;
using GLib;

namespace Hipo
{
	public class HipoMain
	{
		[Widget] private Gtk.Dialog errorDialog;
		[Widget] private Gtk.TextView stackTrace;
		[Widget] private Gtk.Label message;
		
		public static void Main (string[] args)
		{
			Mono.Unix.Catalog.Init (Defines.GETTEXT_PACKAGE, Defines.LOCALE_DIR);
			
			HipoMain main = new HipoMain ();
			main.Run (args);
		}
		
		public void Run (string[] args)
		{
			AppDomain.CurrentDomain.UnhandledException += AppUnhandledExceptionHandler;
			GLib.ExceptionManager.UnhandledException += GlibUnhandledExceptionHandler;
			
			new HipoMainWindow (args);
		}
		
		public void AppUnhandledExceptionHandler (object o, UnhandledExceptionEventArgs args)
		{
			UnhandledExceptionHandler ((Exception)args.ExceptionObject);
		}
		
		public void GlibUnhandledExceptionHandler (GLib.UnhandledExceptionArgs args)
		{
			UnhandledExceptionHandler ((Exception)args.ExceptionObject);
		}
		
		public void UnhandledExceptionHandler (Exception e)
		{
			string stack = GetStackTrace (e);
			string msg = GetMessage (e);
			
			Gtk.Application.Invoke (delegate (object o, EventArgs args) {
				
				Glade.XML gxml = new Glade.XML ("errordialog.glade", "errorDialog");
				
				stackTrace = (TextView)gxml.GetWidget ("stackTrace");
				message = (Label)gxml.GetWidget ("message");
				errorDialog = (Dialog)gxml.GetWidget ("errorDialog");				
				
				stackTrace.Buffer.Text = stack;
				message.Text = msg;
				errorDialog.Run ();
				
				errorDialog.Destroy ();
			});
			
			Application.RunIteration ();
		}
		
		public string GetMessage (Exception e)
		{
			if (e.InnerException != null)
				return String.Concat (e.Message, " : ", GetMessage (e.InnerException));
			else
				return e.Message;
		}
		
		public string GetStackTrace (Exception e)
		{
			if (e.InnerException != null)
				return String.Concat (GetStackTrace (e.InnerException), "\n::End of inner stack trace::\n", e.StackTrace);
			else
				return e.StackTrace;
		}
	}
}
